# A bunch of LaTeX packages

## Content

* messagepassing: A package for message passing diagrams ([available on CTAN](https://www.ctan.org/pkg/messagepassing))
* formal-grammar: A package to write formal grammars ([available on CTAN](https://www.ctan.org/pkg/formal-grammar))
* facture: Une classe pour créer des factures et devis (en français).
* hannot: Changes the layout of the page to increase space for hand-written annotations. Namely, it sets a landscape layout page, while keeping the textwidth of a portrait page.
* randif: A random `if` command.

For the `formal-grammar` and `messagepassing` packages, please refer to the `README.md` in the corresponding subfolder.

## Acknowledgements

* Factures: The class is heavily based on (i.e. "is a translation of") the class [*invoice*](https://github.com/treyhunner/invoices/blob/master/invoice.cls) written by Treyhunner.
* `formal-grammar`: 
  + Thanks to Enrico Gregorio (egreg) for suggesting improvements.
  + Thanks to Fangyi Zhou for the "center |" feature in `formal-grammar` (Commit: #4c3727ef)

## How to use ?

```
Try { 
	If the folder you want contains a .sty file {
		Copy the content of the folder you want in the folder containing your main .tex file
	} else {
		`latex file.ins`
		`pdflatex file.dtx`
		Copy the generated `file.sty` in the folder containing your main .tex file
	}
} catch {
	RTFM
}
```
