% \iffalse meta-comment
% Copyright (C) 2022 by Martin Vassor
% This file may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either
% version 1.3 of this license or (at your option) any later
% version. The latest version of this license is in:
%
%    http://www.latex-project.org/lppl.txt
%
% and version 1.3 or later is part of all distributions of
% LaTeX version 2005/12/01 or later.
% 
% \fi

% \iffalse
%<package> \NeedsTeXFormat{LaTeX2e}
%<package> \ProvidesPackage{randif}[2022/10/19 v1.0 A package to randomly choose among two options]
%<package> \RequirePackage{ifthen}
%<package> \RequirePackage{lcg}
% 

%<*driver>
\documentclass{ltxdoc}
\usepackage{randif}
\usepackage{amsmath}
\usepackage[hidelinks]{hyperref}
\usepackage{listings}
\lstset{
	language=[LaTeX]{TeX},
}
\EnableCrossrefs
\CodelineIndex
\RecordChanges
\begin{document}
  \DocInput{randif.dtx}
\end{document}
%</driver>
% \fi

% \CheckSum{0}
%
% \CharacterTable
%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%   Digits        \0\1\2\3\4\5\6\7\8\9
%   Exclamation   \!     Double quote  \"     Hash (number) \#
%   Dollar        \$     Percent       \%     Ampersand     \&
%   Acute accent  \'     Left paren    \(     Right paren   \)
%   Asterisk      \*     Plus          \+     Comma         \,
%   Minus         \-     Point         \.     Solidus       \/
%   Colon         \:     Semicolon     \;     Less than     \<
%   Equals        \=     Greater than  \>     Question mark \?
%   Commercial at \@     Left bracket  \[     Backslash     \\
%   Right bracket \]     Circumflex    \^     Underscore    \_
%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%   Right brace   \}     Tilde         \~}
%
%
% \changes{v1.0}{2022/10/19}{Initial version for publication}
%
% \GetFileInfo{randif.sty}
%
% \title{The \textsf{randif} package\thanks{This document
%   corresponds to \textsf{randif}~\fileversion,
%   dated \filedate.}}
% \author{Martin Vassor\\ \texttt{bromind+ctan@gresille.org}}
%
%
% \maketitle

% \section{Introduction}
% This package provides the \texttt{randif} command, which randomly choose among
% two options. It also provides a shortcut command to randomly choose between
% showing or hiding a piece of text.
%

%
% \section{Usage}
% \paragraph{Main use.}

% First, consider there is always a probability \(p\) defined. We'll see below
% how to modify it to fit our needs.
% \DescribeMacro{randif}
% The main command of this package is |randif|\marg{then}\marg{else}, which acts
% similarly to the |ifthenelse| command provided by the package \texttt{ifthen},
% except that, instead of the condition, |randif| randomly selects either 
% the \emph{then} branch or the \emph{else} branch; then \emph{then} branch has a probability
% \(p\) to be selected and the \emph{else} branch a probability \(1-p\).


% \DescribeMacro{setproba} Naturally, you may want to change the probability
% \(p\). This is done using the macro |setproba|\marg{threshold}\marg{max}
% allows to do so. The probability \(p\) is then defined as \(\min\left\{1,
% \frac{\text{\emph{threshold}}}{max}\right\}\).
%
% Notice that probabilities are managed using the \texttt{lcg} package under the
% hood. It seems that there is a problem with the way \texttt{lcg}: setting the
% range in which value are randomily choosen is only valid for the scope it is
% defined.
% Therefore, you might experience some inconsistent behaviour. See below for
% debugging. Also, notice that \texttt{lcg} uses a seed that changes with time.
% So if you compile your document to frequently, you might not see differences.

% \DescribeMacro{randshow} Finally, the command |randshow|\marg{text}, which was
% the initial goal of this package. This command simply randomly shows or hide
% the provided \emph{text}. When hidden, the \emph{text} is encapsulated in a
% |phantom| macro, to preserve the space it is supposed to take. This macro was
% initially aimed to create arrays where values are randomly shown.

% \paragraph{Debug help.}

% As said above, the underlying \texttt{lcg} has some strange behaviour with
% its internal values. If you need to understand better what happens, you can
% set the |randif@debug| boolean to |true|, which will display a triplet each
% time a |randif| is called. The first value is the value randomly choosen, the
% second is the threshold, and the last one is the \emph{supposedly max} value.
% If you see that the first value is never within the range you expect (e.g. you
% always observe the first value within \([1, 5]\) while the supposedly max
% value is \(10\), you might experience an issue with scoping, and you might
% need to reset \(p\) using |setproba|.


% \StopEventually{}
%
% \section{Implementation}

%    \begin{macrocode}
\newcounter{randif@counter}
\newcommand{\randif@max}{2}
\newcommand{\randif@threshold}{1}

\newboolean{randif@debug}
\setboolean{randif@debug}{false}

\reinitrand[counter=randif@counter, last=\randif@max]

% Sets the new probability of the first branch
% #1: the threshold below (or equal) which the first branch is taken
% #2: the maximum value
\newcommand{\setproba}[2]{
\renewcommand{\randif@max}{#2}
\renewcommand{\randif@threshold}{#1}
\chgrand[last=\randif@max]
}

% #1: if below or equal to threshold
% #2: if above threshold
\newcommand{\randif}[2]{
	\rand
	\ifthenelse{\boolean{randif@debug}}{
		\(\langle\arabic{randif@counter}, \randif@threshold, \randif@max\rangle\)
	}{}
	\ifthenelse{
		\value{randif@counter} < \randif@threshold
		\OR \value{randif@counter} = \randif@threshold
	}{
		#1
	}{
		#2
	}
}

% #1: the piece of text to show or hide.
\newcommand{\randshow}[1]{
	\randif{#1}{\phantom{#1}}
}
%    \end{macrocode}
