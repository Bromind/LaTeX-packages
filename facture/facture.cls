%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Copyright (c) 2011 Trey Hunner                                          %
%                                                                          %
%  Permission is hereby granted, free of charge, to any person obtaining   %
%  a copy of this software and associated documentation files (the         %
%  "Software"), to deal in the Software without restriction, including     %
%  without limitation the rights to use, copy, modify, merge, publish,     %
%  distribute, sublicense, and/or sell copies of the Software, and to      %
%  permit persons to whom the Software is furnished to do so, subject to   %
%  the following conditions:                                               %
%                                                                          %
%  The above copyright notice and this permission notice shall be          %
%  included in all copies or substantial portions of the Software.         %
%                                                                          %
%  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,         %
%  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF      %
%  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   %
%  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE  %
%  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION  %
%  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION   %
%  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.         %
%                                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ProvidesClass{facture}

\LoadClass[12pt]{article}

\usepackage[letterpaper,hmargin=0.79in,vmargin=0.79in]{geometry}
\usepackage[parfill]{parskip} % Do not indent paragraphs
\usepackage{fp} % Fixed-point arithmetic
\usepackage{calc} % Counters for totaling hours and cost
\usepackage{longtable}
\usepackage{multicol}
\usepackage[utf8]{inputenc}
\usepackage[official]{eurosym}

\pagestyle{empty} % No page numbers
\linespread{1.5}
\def \tab {\hspace*{3ex}}

\setlength{\doublerulesep}{\arrayrulewidth} % Double rules look like one thick one

\newcommand{\tauxTVA}[1]{\def\@tauxTVA{#1}}
\tauxTVA{1.20}

% Command for setting a default hourly rate
\newcommand{\tauxhoraireHT}[1]{
	\def \@tauxhoraireHT {#1}
}
\tauxhoraireHT{1}

\newcommand{\devise}[1]{\def\@devise {#1}}
\devise{\euro{}}

\newcommand{\feetype}[1]{
    \textbf{#1}
    \\
}

% Counters for totaling up hours and dollars
\newcounter{hours} \newcounter{subhours} \newcounter{cost} \newcounter{subcost} \newcounter {totalTTC} \newcounter {souscoutTTC}
\setcounter{hours}{0} \setcounter{subhours}{0} \setcounter{cost}{0} \setcounter{subcost}{0} \setcounter{totalTTC}{0} \setcounter{souscoutTTC}{0}

% Formats inputed number with 2 digits after the decimal place
\newcommand*{\formatNumber}[1]{\FPround{\cost}{#1}{2}\cost} %

% Returns the total of counter
\newcommand*{\total}[1]{\FPdiv{\t}{\arabic{#1}}{1000}\formatNumber{\t}}

% Create an invoice table
\newenvironment{invoiceTable}{
    % Create a new row from title, unit quantity, unit rate, and unit name
    \newcommand*{\unitrow}[4]{%
         \addtocounter{cost}{1000 * \real{##2} * \real{##3}}%
         \addtocounter{subcost}{1000 * \real{##2} * \real{##3}}%
	 ##1 &
	 \formatNumber{##2} ##4 &
	 \formatNumber{##3} \@devise &
	 \FPmul{\cost}{##2}{##3}\formatNumber{\cost} \@devise &
	 	\FPmul{\cost}{##2}{##3}
	 	\FPmul{\coutTTC}{\cost}{\@tauxTVA}
		\addtocounter{souscoutTTC}{1000 * \real{\coutTTC}}
		\addtocounter{totalTTC}{1000 * \real{\coutTTC}}
		\formatNumber{\coutTTC}\@devise%
         \\
    }
    % Create a new row from title and expense amount
    \newcommand*{\feerow}[2]{%
         \addtocounter{cost}{1000 * \real{##2}}%
         \addtocounter{subcost}{1000 * \real{##2}}%
         ##1 & & \$\formatNumber{##2} & \$\FPmul{\cost}{##2}{1}\formatNumber{\cost}%
         \\
    }

    \newcommand{\subtotalNoStar}{
	    {\bf Sous-total} & {\bf \total{subhours} heures} &  & {\bf \total{subcost} \@devise} & {\bf \total{souscoutTTC} \@devise}
        \setcounter{subcost}{0}
        \setcounter{subhours}{0}
	\setcounter{souscoutTTC}{0}
        \\*[1.5ex]
    }
    \newcommand{\subtotalStar}{
        {\bf Subtotal} & & & {\bf \$\total{subcost}}
        \setcounter{subcost}{0}
        \\*[1.5ex]
    }
    \newcommand{\subtotal}{
         \hline
         \@ifstar
         \subtotalStar%
         \subtotalNoStar%
    }


    % Create a new row from date and hours worked (use stored fee type and hourly rate)
    \newcommand*{\hourrow}[2]{%
        \addtocounter{hours}{1000 * \real{##2}}%
        \addtocounter{subhours}{1000 * \real{##2}}%
        \unitrow{##1}{##2}{\@tauxhoraireHT}{heures}%
    }
    \renewcommand{\tabcolsep}{0.8ex}
    \setlength\LTleft{0pt}
    \setlength\LTright{0pt}
    \begin{longtable}{@{\extracolsep{\fill}\hspace{\tabcolsep}} l r r r r}
    \hline
	    {\bf Service} & \multicolumn{1}{c}{\bf Quantité} & \multicolumn{1}{c}{\bf Prix unitaire HT } & \multicolumn{1}{c}{\bf Total HT} &\multicolumn{1}{c}{\bf Total TTC}\\*
    \hline\hline
    \endhead
}{
    \hline\hline\hline
	    {\bf Total} & & & {\bf \total{cost} \@devise} & {\bf\total{totalTTC} \@devise}\\
    \end{longtable}

    \vspace{1cm}
	    Mention \emph{Devis reçu avant l'exécution des travaux} manuscrite :\\
	    Date :\\
	    Signature :\\
	    
    \vfill
}

\newcommand{\validite}[1]{\def\@validite {#1}}
\validite{3 mois}

\newcommand{\siret}[1]{\def \@siret {#1}}
\siret{Aucun numéro siret fourni}

\newcommand{\raisonsociale}[1]{\def\@raison {#1}}

\newcommand{\fournisseur}[4]{
\def\@entreprise {#1}
\def\@adresse {#2}
\def\@codepostal {#3}
\def\@ville{#4}}
\fournisseur{Pas de nom d'entreprise fourni}{}{}{}

\newcommand{\contact}[2]{
	\def\@telephone {#1}
	\def\@email {#2}
}

\newcommand{\client}[4]{
\def\@centreprise {#1}
\def\@cadresse {#2}
\def\@ccodepostal {#3}
\def\@cville{#4}}
\client{Pas de nom de client fourni}{}{}{}

\renewcommand{\maketitle}{
	\hfil{\Huge\bf Devis}\hfil
\break
\hrule
\begin{multicols}{2}
	{\bf Fournisseur :}\\
	\tab \@entreprise, \@raison\\
	\tab \@adresse\\
	\tab \@codepostal\ \textsc{\@ville}\\
	\tab Siret : \@siret\\
	\tab Téléphone : \@telephone\\
	\tab Email : \@email\\

	\columnbreak
	{\bf Client :}\\
	\tab \@centreprise\\
	\tab \@cadresse\\
	\tab \@ccodepostal\ \textsc{\@cville}\\ \\
	{\bf Le : }\@date\\
	{\bf Devis valable : }\@validite\\
\end{multicols}
}
